#include <ros/ros.h>

#include <assembly_control_ros/assembly_station_state.h>
#include <assembly_control_ros/assembly_station_command.h>
#include <assembly_control_ros/assembly_station_input.h>
#include <assembly_control_ros/assembly_station_output.h>

#include <common/machine_controller.hpp>

class AssemblyStation
    : public MachineController<assembly_control_ros::assembly_station_state,
                               assembly_control_ros::assembly_station_input,
                               assembly_control_ros::assembly_station_command,
                               assembly_control_ros::assembly_station_output > {
public:
    AssemblyStation(ros::NodeHandle node)
        : MachineController(node, "assembly_station"), state_(State::OFF) {
    }

    virtual void process() override {
        assembly_control_ros::assembly_station_command commands;
        assembly_control_ros::assembly_station_output outputs;
       assembly_control_ros::assembly_station_input input ;
        auto& inputs = getInputs();

        switch (state_) {
        case State::OFF:
            
            if (getState().valid) {
                outputs.assemblage_complet = true;
                sendOuputs(outputs);
                state_ = State::check;
            }
            break;
        case State::check:
            commands.check= true;
            if (getState().evacuated) {
                commands.check= false;
                state_= State::OFF;
            }
            break;
        }

        sendCommands(commands);
    }

private:
    enum class State { check,OFF};

    State state_;
};

int main(int argc, char* argv[]) {
    ros::init(argc, argv, "assembly_station");

    ros::NodeHandle node;

    ros::Rate loop_rate(50); // 50 Hz

    AssemblyStation conveyor(node);

    while (ros::ok()) {
        conveyor.process();

        ros::spinOnce();

        loop_rate.sleep();
    }
}