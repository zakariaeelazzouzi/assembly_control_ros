#include <ros/ros.h>

#include <assembly_control_ros/robot_state.h>
#include <assembly_control_ros/robot_command.h>
#include <assembly_control_ros/robot_input.h>
#include <assembly_control_ros/robot_output.h>


#include <common/machine_controller.hpp>

class Robot: public MachineController < assembly_control_ros::robot_state,
                               assembly_control_ros::robot_input,
                               assembly_control_ros::robot_command,
                               assembly_control_ros::robot_output> {

bool part_available = false ;
public:
    Robot(ros::NodeHandle node)
        : MachineController(node, "robot"), state_(State::wait){
    }
    
    virtual void process() override {
        
        assembly_control_ros::robot_command commands;
        assembly_control_ros::robot_output outputs;
        //assembly_control_ros::robot_input inputs;
        

        auto& states = getState();
        auto& inputs = getInputs();



switch (state_) {

    case State::wait:
              
          ROS_INFO("[robot] Arrete");

          if (inputs.part_available )
                 {   
                     state_ = State::move_right;
                     sendCommands(commands);
                     ROS_INFO("[robot] le robot se déplace a droit_assembly_convy ");
                 }
                 
          break;

      case State::grasp:
            
             ROS_INFO("[robot] grasp");

                commands.grasp= true;
                sendCommands(commands);
                ros::Duration(0.5).sleep();
          
          if (getState().part_grasped) 
                  {
                         if ((inputs.part1) && ~(getState().part1_assembled))
                    
                         {
                            state_ = State::move_left; 
                         }
                    
                        if ((inputs.part2) && ~(getState().part2_assembled ))
                         {
                           state_ = State::move_left;
                         }
                   
                         if ((inputs.part3) && ~(getState().part3_assembled))
                     
                         {
                           state_ = State::move_left;
                         }
                    
                         if ((inputs.part1) && (getState().part1_assembled ))
                         {
                                state_ = State::move_right;
                                outputs.part_vers_eva=true ;
                                sendOuputs(outputs); 
                          }
                          if ((inputs.part2) && (getState().part2_assembled ))
                   
                           {
                               state_ = State::move_right;
                                outputs.part_vers_eva=true ;
                                sendOuputs(outputs); 
                            }
                            if ((inputs.part3) && (getState().part3_assembled ))
                    
                           {
                                 state_ = State::move_right;
                                 outputs.part_vers_eva=true ;
                                 sendOuputs(outputs); 
                     
                             }
                    }
      break;
         

       case State:: move_left :
        
             ROS_INFO("[robot] move_left");


               if (getState().at_evacuation_conveyor)
               {    
                   commands.move_left= true;
                   sendCommands(commands);
                   ros::Duration(2).sleep();
                   ROS_INFO("[robot] le robot se déplace a gauche ");
                     
                      if ((getState().at_supply_conveyor) && (inputs.part_available ) )
                   

                          { commands.move_left= false;
                            sendCommands(commands);
                            ros::Duration(2).sleep();
                           
                          }


                          if  ((getState().at_supply_conveyor) && ~(inputs.part_available)) {
                     
                             if (getState().at_assembly_station) 
                            {
                              commands.move_left=false;
                              sendCommands(commands);
                              ros::Duration(2).sleep();
                              ROS_INFO("[robot] le robot en possition de repos  ");
                             }
                             }
                          }
                  if ((getState().at_supply_conveyor) &&(getState().part_grasped)) 
                  {
                         
                          commands.move_left= true;
                          sendCommands(commands);
                          ros::Duration(0.5).sleep();
                          ROS_INFO("[robot] le robot se déplace a gauche_assembly_convy ");
                 
                                 if (getState().at_assembly_station)
                                  {
                                    commands.move_left= false;
                                    sendCommands(commands);
                                    ros::Duration(2).sleep();
                                   }

                                 if (inputs.part1)
                                  
                                  {
                                    state_ = State::assemble_part1;
                                    ROS_INFO("[robot] part1");
                                   }

                                if  (inputs.part2)
                                  {
                                   state_ = State::assemble_part2;
                 
                                   ROS_INFO("[robot] part2");
                                   }

                                   if   (inputs.part3)
                                   {
                                     sendCommands(commands);
                                     ROS_INFO("[robot] part3");
                                    state_ = State::assemble_part3;
                                   }
                       }
        break;
            

         case State:: move_right:
           
                
                commands.move_right=true;
                sendCommands(commands);
                ros::Duration(2).sleep();

            if (getState().at_assembly_station)

                    {
                  commands.move_right=true;
                   sendCommands(commands);
                   ROS_INFO("[robot] le robot se déplace a droit_supply_conveyor ");

                             if (getState().at_supply_conveyor)
                                    {
                                     commands.move_right = false ;
                                     sendCommands(commands);
                                     ros::Duration(0.5).sleep();
                                      state_ = State:: grasp ;
                                    }
                    }
           
            if (getState().at_supply_conveyor) 
               {
                   commands.move_right=true;
                   sendCommands(commands);
                   ROS_INFO("[robot] le robot se déplace a droit_evacuation");
                  if (getState().at_evacuation_conveyor)
                   {
                     commands.move_right = false ;
                     sendCommands(commands);
                        state_ = State:: release ;
                    }
                
                }            
            
         break;

        case State:: assemble_part1:

            commands.assemble_part1= true ;
            sendCommands(commands);
            if (getState().part1_assembled)

            {
              commands.assemble_part1= false ;
              sendCommands(commands);
              
            }
        break;

        case State:: assemble_part2:

            commands.assemble_part2= true ;
            sendCommands(commands);

            if (getState().part2_assembled)
            {
              commands.assemble_part2= false ;
              sendCommands(commands);
              
            }
            break;

        case State::assemble_part3 :
            commands.assemble_part3= true ;
            sendCommands(commands);
            if (getState().part3_assembled)
            {
              commands.assemble_part3= false ;
              sendCommands(commands); 
            
            }
              

            break;

          case State:: release :
          
             commands.release= true ;
             sendCommands(commands);
             if   (getState().part_released)
             {
                  commands.release= false ;
                  sendCommands(commands);
                  outputs.piece_released=true ;
                  sendOuputs(outputs);  

                  state_ = State::move_left;
             }
             
             
            break; 
  sendCommands(commands);
        }
        
                               
        
                               }
private:
    enum class State { wait,grasp,release,assemble_part3,assemble_part2,assemble_part1,move_right,move_left};

    State state_;

};
int main(int argc, char* argv[]) {
    ros::init(argc, argv, "robot");

    ros::NodeHandle node;

    ros::Rate loop_rate(50); // 50 Hz

    Robot robot(node);

    while (ros::ok()) {
        robot.process();

        ros::spinOnce();

        loop_rate.sleep();
    }
}
