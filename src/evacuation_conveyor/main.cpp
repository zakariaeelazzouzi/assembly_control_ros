#include <ros/ros.h>

#include <assembly_control_ros/evacuation_conveyor_state.h>
#include <assembly_control_ros/evacuation_conveyor_command.h>
#include <assembly_control_ros/evacuation_conveyor_input.h>
#include <assembly_control_ros/evacuation_conveyor_output.h>

#include <common/machine_controller.hpp>

class EvacuationConveyor
    : public MachineController<assembly_control_ros::evacuation_conveyor_state,
                               assembly_control_ros::evacuation_conveyor_input,
                               assembly_control_ros::evacuation_conveyor_command,
                               assembly_control_ros::evacuation_conveyor_output> {
    
    private:
    enum class State { On,off};

    State state_;
public:
    EvacuationConveyor(ros::NodeHandle node)
        : MachineController(node, "evacuation_conveyor"), state_(State::On) {
    }



    virtual void process() override {
        assembly_control_ros::evacuation_conveyor_command commands;
        assembly_control_ros::evacuation_conveyor_output outputs;

        auto& inputs = getInputs();

        switch (state_) {
        case State::On:
            commands.on = true;
                 if ( inputs.part_vers_eva ) 
                     {
                
                      commands.on = false ;
                      state_ = State::off;
                     }
        break;

        case State::off:
              
                if (inputs.piece_released) 
                 {
                   state_ = State::On;
                 }
        break;
                 
        }
        sendCommands(commands);
        }
              
  //private:
    //enum class State { Wait, Processing, Ready };

    //State state_;

};


int main(int argc, char* argv[]) {
    ros::init(argc, argv, "evacuation_conveyor");

    ros::NodeHandle node;

    ros::Rate loop_rate(50); // 50 Hz

  EvacuationConveyor conveyor(node);

    while (ros::ok()) {
        conveyor.process();

        ros::spinOnce();

        loop_rate.sleep();
    }
}